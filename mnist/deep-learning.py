# Importiert notwendige Biliotheken
from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import numpy as np
import os
from plotting import *

# Festlegen eines Pfades, an dem Dateien gespeichert und von dem Dateien abgerufen werden (Ist anzupassen)
env_path = "/Users/Lollo/thesis-ml/mnist"

# Download und Einlesen der MNIST-Daten
mnist = input_data.read_data_sets(env_path + '/data/', one_hot=True)

# Hilfsvariablen
trainset_size   = mnist.train.num_examples
testset_size    = mnist.test.num_examples

# Variablen
epochs = 5
classes = 10
features = 784
layer_nodes = [features, 400, 200, 400, classes]
stddev = 0.050
learning_rate = 5e-4
batch_size = 32
bias_wights_init = 0.050
epoch_errors = []

# Platzhalter für TensorFlow
x = tf.placeholder(dtype=tf.float32, shape=[None, features], name='x')
y = tf.placeholder(dtype=tf.float32, shape=[None, classes], name='y')

# Gewichte
W1 = tf.Variable(tf.truncated_normal([layer_nodes[0], 
                                        layer_nodes[1]], 
                                        stddev=stddev), name='W1')
W2 = tf.Variable(tf.truncated_normal([layer_nodes[1], 
                                        layer_nodes[2]], 
                                        stddev=stddev), name='W2')
W3 = tf.Variable(tf.truncated_normal([layer_nodes[2], 
                                        layer_nodes[3]], 
                                        stddev=stddev), name='W3')
W4 = tf.Variable(tf.truncated_normal([layer_nodes[3], 
                                        layer_nodes[4]], 
                                        stddev=stddev), name='W4')
# Bias Neuronen
b1 = tf.Variable(tf.constant(bias_wights_init, shape=[layer_nodes[1]]), name='b1')
b2 = tf.Variable(tf.constant(bias_wights_init, shape=[layer_nodes[2]]), name='b2')
b3 = tf.Variable(tf.constant(bias_wights_init, shape=[layer_nodes[3]]), name='b3')
b4 = tf.Variable(tf.constant(bias_wights_init, shape=[layer_nodes[4]]), name='b4')

# Netzwerk erstellen
def model(x):
    input_layer     =    {'weights': W1, 'biases': b1}
    hidden_layer_1  =    {'weights': W2, 'biases': b2}
    hidden_layer_2  =    {'weights': W3, 'biases': b3}
    output_layer    =    {'weights': W4, 'biases': b4}

    input_layer_sum = tf.add(tf.matmul(x, input_layer['weights']), input_layer['biases'])
    input_layer_sum = tf.nn.relu(input_layer_sum)

    hidden_layer_1_sum = tf.add(tf.matmul(input_layer_sum, hidden_layer_1['weights']), hidden_layer_1['biases'])
    hidden_layer_1_sum = tf.nn.relu(hidden_layer_1_sum)

    hidden_layer_2_sum = tf.add(tf.matmul(hidden_layer_1_sum, hidden_layer_2['weights']), hidden_layer_2['biases'])
    hidden_layer_2_sum = tf.nn.relu(hidden_layer_2_sum)

    output_layer_sum = tf.add(tf.matmul(hidden_layer_2_sum, output_layer['weights']), output_layer['biases'])
    return output_layer_sum

# Trainieren & Testen
def train(x):
    pred = model(x)
    pred = tf.identity(pred)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=pred, labels=y))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)

    with tf.Session() as sess:
    
        # Initialisieren der Modell-Variablen (Gewichte / Bias Neuronen)
        sess.run(tf.global_variables_initializer())

        # Training
        for epoch in range(epochs):
            epoch_loss = 0.0
            for i in range(int(trainset_size / batch_size) + 1):
                epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                _, c = sess.run([optimizer, cost], feed_dict={x: epoch_x, y: epoch_y})
                epoch_loss += c
            epoch_errors.append(epoch_loss)
            print('epoch: ', epoch + 1, ' of ', epochs, ' with loss: ', epoch_loss)
        
        # Durch das Auskommentieren der folgenden Zeile wird die Entwicklung des Fehlerwertes in einem Diagramm dargestellt
        #display_convergence(epoch_errors)

        # Test
        correct_result = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_result, 'float'))
        accuracy = accuracy.eval({x:mnist.test.images, y:mnist.test.labels})
        print('Accuracy: ', accuracy)

        # Speichert das Modell
        saver = tf.train.Saver()
        tf.saved_model.simple_save(sess,
            env_path+"/data/model",
            inputs={"x": x},
            outputs={"y": y})
        save_path = saver.save(sess, env_path + "/data/model.ckpt")


# Aufruf des Trainings & Testen
train(x)