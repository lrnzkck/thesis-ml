# Imports
from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import numpy as numpy
import os
from plotting import *
import random

# Festlegen eines Pfades, an dem Dateien gespeichert und von dem Dateien abgerufen werden (Ist anzupassen)
env_path = "/Users/Lollo/thesis-ml/mnist"

# Laden des Datensatzes
mnist = input_data.read_data_sets(env_path+'/data/', one_hot=True)
test_size = mnist.test.num_examples

# Variablen
classes = 10
features = 784
layer_nodes = [features, 400, 200, 400, classes]
stddev = 0.1

loaded_graph = tf.Graph()
with loaded_graph.as_default():
    # Platzhalter
    x = tf.placeholder(dtype=tf.float32, shape=[None, features], name='x')
    y = tf.placeholder(dtype=tf.float32, shape=[None, classes], name='y')

    # Gewichtungen
    W1 = tf.Variable(tf.truncated_normal([layer_nodes[0], 
                                        layer_nodes[1]], 
                                        stddev = stddev), 
                                        name="W1")

    W2 = tf.Variable(tf.truncated_normal([layer_nodes[1], 
                                        layer_nodes[2]], 
                                        stddev = stddev), 
                                        name="W2")

    W3 = tf.Variable(tf.truncated_normal([layer_nodes[2], 
                                        layer_nodes[3]], 
                                        stddev = stddev), 
                                        name="W3")

    W4 = tf.Variable(tf.truncated_normal([layer_nodes[3], 
                                        layer_nodes[4]], 
                                        stddev = stddev), 
                                        name="W4")

    # Bias Neuronen
    b1 = tf.Variable(tf.truncated_normal(shape=[layer_nodes[1]],
                                        stddev = stddev), name="b1" )

    b2 = tf.Variable(tf.truncated_normal(shape=[layer_nodes[2]],
                                        stddev = stddev), name="b2" )

    b3 = tf.Variable(tf.truncated_normal(shape=[layer_nodes[3]],
                                        stddev = stddev), name="b3" )

    b4 = tf.Variable(tf.truncated_normal(shape=[layer_nodes[4]],
                                        stddev = stddev), name="b4" )

    # Saver-Objekt für das Reproduzieren des Modells
    saver = tf.train.Saver({"W1": W1, "W2": W2, "W3": W3, "W4": W4,
                            "b1": b1, "b2": b2, "b3": b3, "b4": b4, })

# Modell definieren, um es später mit Werten zu füllen
def model(x):
    input_layer     = {'weights': W1, 'biases': b1}
    hidden_layer_1  = {'weights': W2, 'biases': b2}
    hidden_layer_2  = {'weights': W3, 'biases': b3}
    output_layer    = {'weights': W4, 'biases': b4}

    input_layer_sum = tf.add(tf.matmul(x, input_layer['weights']),
                            input_layer['biases'])
    input_layer_sum = tf.nn.relu(input_layer_sum)

    hidden_layer_1_sum = tf.add(tf.matmul(input_layer_sum, hidden_layer_1['weights']),
                            hidden_layer_1['biases'])
    hidden_layer_1_sum = tf.nn.relu(hidden_layer_1_sum)

    hidden_layer_2_sum = tf.add(tf.matmul(hidden_layer_1_sum, hidden_layer_2['weights']),
                            hidden_layer_2['biases'])
    hidden_layer_2_sum = tf.nn.relu(hidden_layer_2_sum)
       
    output_layer_sum = tf.add(tf.matmul(hidden_layer_2_sum, output_layer['weights']),
                            output_layer['biases'])
   
    return output_layer_sum

def test():
    with tf.Session(graph=loaded_graph) as sess:
        saver = tf.train.import_meta_graph(env_path +"/data/model.ckpt.meta")
        saver.restore(sess, env_path +"/data/model.ckpt")

        # Lade Tensoren
        x = loaded_graph.get_tensor_by_name('x:0')
        y = loaded_graph.get_tensor_by_name('y:0')

        # Teste das geladene Modell
        for i in range(10):
            rand_index = random.randint(1, test_size)
            test_image = mnist.test.images[rand_index]
            test_label = mnist.test.labels[rand_index]
            pred = model(np.reshape(test_image, (1, 784)))
            max_val = tf.argmax(pred, 1)
            display_digit(test_image, test_label, sess.run(max_val))

test()  